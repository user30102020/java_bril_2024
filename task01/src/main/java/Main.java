import models.User;
import repositories.EventsRepository;
import repositories.UsersRepository;
import repositories.impl.EventsRepositoryFileImpl;
import repositories.impl.UsersRepositoryFileImpl;
import services.AppService;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("data/users.txt");
        EventsRepository eventsRepository = new EventsRepositoryFileImpl("data/events.txt", "data/events_users.txt");
        AppService appService = new AppService(usersRepository, eventsRepository);

//        appService.signUp("admin@gmail.com", "qwerty007");
//        appService.signUp("max@gmail.com", "sdaf");

//        appService.addEvent("Практика по Java", LocalDate.now());
//        appService.addEvent("Практика по Golang", LocalDate.now().plusDays(1));

//        appService.addUserToEvent("max@gmail.com", "Практика по Java");
//        appService.addUserToEvent("max@gmail.com", "Практика по Golang");
//        appService.addUserToEvent("admin@gmail.com", "Практика по Golang");

//        System.out.println(appService.getAllEventsByUser("admin@gmail.com"));
//        System.out.println(appService.getAllEventsByUser("max@gmail.com"));
    }
}
