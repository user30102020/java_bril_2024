import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import models.Course;
import models.Student;
import models.repositories.CoursesRepository;
import models.repositories.StudentsRepository;
import models.repositories.impl.CoursesRepositoryJdbsImpl;
import models.repositories.impl.StudentsRepositoryJdbsImpl;

import java.sql.Date;

public class Main {
    public static void main(String[] args) {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl("jdbc:postgresql://localhost:5432/taxidb");
        hikariConfig.setUsername("postgres");
        hikariConfig.setPassword("n0tDrivingDrunk118");
        hikariConfig.setDriverClassName("org.postgresql.Driver");

        HikariDataSource dataSource = new HikariDataSource(hikariConfig);
        StudentsRepository studentsRepository = new StudentsRepositoryJdbsImpl(dataSource);

        Student student = Student.builder()
                .firstName("Имя1")
                .lastName("Фамилия1")
                .age(25)
                .build();

        System.out.println(student);
        studentsRepository.save(student);
        System.out.println(student);

        System.out.println(studentsRepository.findAll());

        CoursesRepository coursesRepository = new CoursesRepositoryJdbsImpl(dataSource);

        Course course = Course.builder()
                .title("Курс1")
                .startDate(Date.valueOf("2024-01-01"))
                .finishDate(Date.valueOf("2025-01-01"))
                .build();
        System.out.println(course);
        coursesRepository.save(course);
        System.out.println(course);

        System.out.println(coursesRepository.findAll());
    }
}
