package models.repositories;

import models.Student;

public interface StudentsRepository extends CrudRepository<Student> {
}
