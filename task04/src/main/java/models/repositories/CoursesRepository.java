package models.repositories;

import models.Course;

public interface CoursesRepository extends CrudRepository<Course> {
}
