import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import models.Course;
import models.Student;
import models.repositories.CoursesRepository;
import models.repositories.StudentsRepository;
import models.repositories.impl.CoursesRepositoryJdbsImpl;
import models.repositories.impl.spring.CoursesRepositorySpringJdbsImpl;
import models.repositories.impl.spring.StudentsRepositorySpringJdbcImpl;

import java.sql.Date;

public class Main {
    public static void main(String[] args) {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl("jdbc:postgresql://localhost:5432/taxidb");
        hikariConfig.setUsername("postgres");
        hikariConfig.setPassword("n0tDrivingDrunk118");
        hikariConfig.setDriverClassName("org.postgresql.Driver");

        HikariDataSource dataSource = new HikariDataSource(hikariConfig);

//        StudentsRepository studentsRepository = new StudentsRepositorySpringJdbcImpl(dataSource);
//
//        Student student = Student.builder()
//                .firstName("Имя2")
//                .lastName("Фамилия2")
//                .age(33)
//                .build();
//
//        System.out.println(student);
//        studentsRepository.save(student);
//        System.out.println(student);
//
//        System.out.println(studentsRepository.findAll());

        CoursesRepository coursesRepository = new CoursesRepositorySpringJdbsImpl(dataSource);

        Course course = Course.builder()
                .title("Курс2")
                .startDate(Date.valueOf("2023-05-06"))
                .finishDate(Date.valueOf("2024-06-05"))
                .build();

        System.out.println(course);
        coursesRepository.save(course);
        System.out.println(course);
        System.out.println(coursesRepository.findAll());
    }
}
