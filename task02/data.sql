insert into product (name, price) values ('Печенье', 56),
                                         ('Молоко', 66),
                                         ('Сыр', 140),
                                         ('Чулки', 900),
                                         ('Кабачок', 25),
                                         ('Червячок', 3);

insert into consumer (first_name, last_name, age, company) values ('Кирилл', 'Чулочкин', 33, 'Три бибизяна'),
                                                                  ('Мефодий', 'Сеточкин', 40, 'Сушим воблу');

insert into shop (name, address) values ('Цвет сметаны', 'ул. Академика Широкопояса, 69'),
                                        ('Зеленый горошек', 'ул. Спицына, 4'),
                                        ('Мышка-сосиска', 'ул. Морская, 8');

insert into product_in_shop (shop_id, product_id) values (1, 3),
                                                         (1, 2),
                                                         (2, 1),
                                                         (2, 5),
                                                         (2, 3),
                                                         (3, 4),
                                                         (3, 6);

insert into orders (shop_id, product_id, consumer_id) values (3, 4, 2),
                                                             (3, 6, 2),
                                                             (2, 5, 2),
                                                             (1, 3, 1),
                                                             (2, 3, 1);