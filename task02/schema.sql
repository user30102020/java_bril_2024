create table product (
                         id serial primary key,
                         name varchar(20),
                         price integer
);

create table consumer (
                          id serial primary key,
                          first_name varchar(20),
                          last_name varchar(20),
                          age integer check ( age > 18 and age < 100),
                          company varchar(20)
);

create table shop (
                      id serial primary key,
                      name varchar(20),
                      address varchar(40)
);

create table product_in_shop (
                                 shop_id integer,
                                 product_id integer,
                                 foreign key (shop_id) references shop(id),
                                 foreign key (product_id) references product(id)
);

create table orders (
                        shop_id integer,
                        product_id  integer,
                        consumer_id integer,
                        foreign key (shop_id) references shop(id),
                        foreign key (product_id) references product(id),
                        foreign key (consumer_id) references consumer(id)
);